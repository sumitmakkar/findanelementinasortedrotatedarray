#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        vector<int> arrVector;
        vector<int> rotatedVector;
        int         rotation;
        int         elementToBeSearched;
    
        void rotateVector(int rot)
        {
            int len = (int)arrVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                rotatedVector.push_back(arrVector[(i+rot)%len]);
            }
        }
    
        void display()
        {
            int len = (int)rotatedVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                cout<<rotatedVector[i]<<" ";
            }
            cout<<endl;
        }
    
        int findPivotIndex()
        {
            int start = 0;
            int end   = (int)rotatedVector.size() - 1;
            while (start < end)
            {
                int mid = (start + end) / 2;
                if(rotatedVector[mid] < rotatedVector[mid-1])
                {
                    return mid;
                }
                if(rotatedVector[end-1] > rotatedVector[end])
                {
                    return end;
                }
                if(!rotation)
                {
                    return start;
                }
                if(rotatedVector[start] <= rotatedVector[mid])
                {
                    start = mid;
                }
                else
                {
                    end = mid - 1;
                }
            }
            return -1;
        }
    
        int binarySearch(int start , int mid , int end)
        {
            while(start < mid)
            {
                if(rotatedVector[mid] == elementToBeSearched)
                {
                    return mid;
                }
                if(rotatedVector[start] == elementToBeSearched)
                {
                    return start;
                }
                if(rotatedVector[end] == elementToBeSearched)
                {
                    return end;
                }
                
                if(rotatedVector[mid] < elementToBeSearched)
                {
                    start = mid + 1;
                }
                else
                {
                    end = end - 1;
                }
            }
            return -1;
        }
    
    public:
        Engine(vector<int> aV , int rot , int elTBS)
        {
            arrVector           = aV;
            rotation            = rot%(int)arrVector.size();
            elementToBeSearched = elTBS;
            rotateVector(rotation);
        }
    
        int findElement()
        {
            int start = 0;
            int end   = (int)rotatedVector.size() - 1;
            int mid   = findPivotIndex();
            if(rotatedVector[mid] == elementToBeSearched)
            {
                return mid;
            }
            else if(rotatedVector[mid] > rotatedVector[start])
            {
                end = mid - 1;
            }
            else
            {
                start = mid + 1;
            }
            return binarySearch(start , mid , end);
        }
};

int main(int argc, const char * argv[])
{
    vector<int> arrVector = {3 , 5 , 6 , 10 , 13 , 21 , 28 , 50};
    Engine      e         = Engine(arrVector , 2 , 13);
    cout<<e.findElement()<<endl;
    return 0;
}
